from flask import Blueprint, make_response, jsonify, send_file

from .forms import FileUploadForm
from .services import CompareFacade
from app import Swagger

comparer = Blueprint('compare', __name__, url_prefix='/compare')


@comparer.route('/', methods=['POST'])
def compare_action():
    """
    Compares audio file with musicXml file
    ---
    parameters:
    - name: audio_file
      required: true
      in: formData
      type: file
      description: Audio file in wav format
    - name: music_xml_file
      required: true
      in: formData
      type: file
      description: MusicXML file
    - name: bpm
      required: true
      in: formData
      type: integer
      description: BPM
    - name: measure_to
      required: false
      in: formData
      type: integer
      description: starting measure (currently turned off)
      parameters:
    - name: measure_from
      required: false
      in: formData
      type: integer
      description: ending measure (currently turned off)
    responses:
      200:
        description: Please wait the calculation, you'll receive an MusicXml File containing errors
    """
    form = FileUploadForm()
    service = CompareFacade()

    if form.validate_on_submit():

        mxml_with_errors = service.run(form.attributes())

        return send_file(mxml_with_errors, as_attachment=True)
    else:
        return make_response(jsonify(errors=form.errors))