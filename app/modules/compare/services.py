from app.modules.processors import MXMLProcessor, AudioProcessor, \
    CompareProcessor
from .forms import FormAttributesVO


class CompareFacade(object):
    def __init__(self):
        self.audio_processor = AudioProcessor()
        self.mxml_processor = MXMLProcessor()
        self.compare_processor = CompareProcessor()

    def run(self, attributes: FormAttributesVO) -> str:
        audio_file = self.audio_processor.process(attributes.audio_file)
        midi_file = self.mxml_processor.process(
            attributes.mxml_file,
            attributes.bpm)

        err_notes, err_durations = self.compare_processor.compare(
            audio_file_path=audio_file,
            midi_file_path=midi_file
        )

        return self.mxml_processor.apply_errors(err_notes, err_durations)
