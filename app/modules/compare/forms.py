import os

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from werkzeug.utils import secure_filename
from wtforms import IntegerField
from wtforms.validators import DataRequired, NumberRange

from app import app


class FormAttributesVO(object):
    def __init__(self,
                 audio_file: str,
                 mxml_file: str,
                 bpm: int = 120,
                 measure_from: int = None,
                 measure_to=None):
        self.audio_file = audio_file
        self.mxml_file = mxml_file
        self.bpm = bpm
        self.measure_from = measure_from
        self.measure_to = measure_to


class FileUploadForm(FlaskForm):
    UPLOAD_FOLDER = 'uploads'

    audio_file = FileField('audio_file', validators=[
        FileRequired(),
        FileAllowed({'wav'}, 'Audio file should have *.wav format')
    ])
    music_xml_file = FileField('music_xml_file', validators=[
        FileRequired(),
        FileAllowed({'mxl'}, 'Only mxl files are supported')
    ])
    bpm = IntegerField('bpm', validators=[
        DataRequired(),
        NumberRange(20, 300, 'BPM should be in range 20 - 300')
    ])

    measure_from = IntegerField('measure_from')
    measure_to = IntegerField('measure_to')

    def __store_file(self, file) -> str:
        filename = secure_filename(file.filename)
        path = os.path.join(
            app.instance_path,
            self.UPLOAD_FOLDER,
            filename)
        file.save(path)

        return path

    def attributes(self) -> FormAttributesVO:
        return FormAttributesVO(
            audio_file=self.__store_file(self.audio_file.data),
            mxml_file=self.__store_file(self.music_xml_file.data),
            bpm=self.bpm.data,
            measure_to=self.measure_to.data,
            measure_from=self.measure_from.data
        )
