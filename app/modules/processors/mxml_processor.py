import os

import pretty_midi
from midi2audio import FluidSynth
from music21 import tempo
from music21.converter import parse
from music21.repeat import ExpanderException
from music21.stream import Stream, PartStaff, Measure, repeat


class MeasureTact(object):
    def __init__(self,
                 id: int = 0,
                 starts: float = .0,
                 duration=.0,
                 ends: float = .0,
                 staff: int = 0):
        self.id = id
        self.starts = starts
        self.duration_in_ms = duration
        self.ends = ends
        self.staff = staff


class Note(object):
    def __init__(self,
                 type: str = 'pause',
                 id: int = 0,
                 starts: float = .0,
                 ends: float = .0,
                 measure: int = 0):
        self.type = type
        self.id = id
        self.starts = starts
        self.ends = ends
        self.measure = measure
        self.pitches = ''

    @property
    def pitches(self) -> str:
        return self._pitches

    @pitches.setter
    def pitches(self, pitches_string: str):
        self._pitches = pitches_string.replace('-', '')

    def has_in_pitches(self, note: str):
        return note in self._pitches


class MXMLProcessor(object):
    def __init__(self):
        self.stream = None
        self.filepath = ''
        self.measures = {}
        self.notes = list

    AUDIO_FS = 22050

    def process(self, filepath: str, bpm: int):
        self.filepath = filepath
        midi_file = self.convert_to_midi(filepath, bpm)
        return self.convert_to_wav(midi_file)

    def _reveal_notes(self, stream: Stream, bpm: int):
        """
        Runs through all the streams and measures to reveal notes
        :param stream: Stream
        :param bpm: int
        :return: dict
        """
        measures = {}
        notes = []
        for element in stream:
            if isinstance(element, PartStaff):
                notes, measures = self._run_through_measures(element, bpm,
                                                             notes,
                                                             measures)
        self.measures = measures
        self.notes = sorted(notes, key=lambda i: i.starts)

    def __expand_stream(self, stream: Stream):
        """
        Find all repeatings and expand stream
        :param stream: Stream
        :return: Stream
        """
        try:
            expander = repeat.Expander(stream)
            song = expander.process()
        except ExpanderException as ex:
            song = stream
        return song

    def _run_through_measures(self, staffStream: PartStaff, bpm: int,
                              notes: list, measures: dict):
        """
        Runs through measures to get all the notes from part staff
        :param staffStream: PartStaff
        :param bpm: int
        :param measures: dict
        :return: tuple
        """
        # if in the song can be repeats we should process them
        song = self.__expand_stream(staffStream)

        total_duration = .0
        ms_per_beat = 60000.0 / float(bpm)

        for measureStream in song:
            if not measureStream.isStream:
                continue

            for element in measureStream:
                if isinstance(element, tempo.MetronomeMark):
                    ms_per_beat = 60000.0 / float(
                        element.getQuarterBPM())

            measureTact = MeasureTact(
                id=measureStream.id,
                starts=total_duration * ms_per_beat
            )

            measure_duration, notes = \
                self.__run_through_notes(
                    measureStream,
                    ms_per_beat,
                    measureTact.starts,
                    notes
                )

            total_duration += measure_duration
            measureTact.duration_in_ms = measure_duration * ms_per_beat
            measureTact.ends = measureTact.starts + measureTact.duration_in_ms

            measures[measureStream.number] = measureTact
            return notes, measures

    def __run_through_notes(self,
                            measure: Measure,
                            milliseconds_per_beat: float,
                            starts: float,
                            notes: list):
        """

        :param measure: Measure
        :param milliseconds_per_beat: float
        :param starts: float (second that it starts)
        :param notes: list[Note]
        :return: tuple
        """
        measure_duration = .0
        for note in measure.recurse().notesAndRests:
            length = float(note.duration.quarterLength) \
                if note.duration.quarterLength else 0.0

            measure_duration += length
            ends = length * milliseconds_per_beat

            element = Note(
                starts=starts,
                ends=starts + ends,
                id=note.id,
                measure=measure.number)

            if note.isNote:
                element.type = 'note'
                element.pitches = str(note.pitch)

            if note.isChord:
                element.type = 'chord'
                element.pitches = ' '.join(
                    [str(x.pitch) for x in note._notes])

            notes.append(element)
            starts += ends * milliseconds_per_beat

        return measure_duration, notes

    def inject_errors(self, notes_issues: list, duration_errors: dict):
        """
        Appends note errors and tempo errors found to stream
        :param notes_issues: dict
        :param duration_errors: dict
        :return: Stream
        """
        for part in self.stream:
            if not part.isStream:
                continue
            for measure in part:
                if not isinstance(measure, Measure):
                    continue
                # if measure.number in tempo_issues.keys():
                #     measure.removeByClass(TextExpression)
                #     measure.append(TextExpression(tempo_issues[measure.number]))
                for a in measure.elements:
                    if a.id in notes_issues:
                        a.addLyric('er')

    def map_duration_errors_to_measures(self, duration_errors) -> dict:
        """
        Maps duration errors to object that contain measure
        id where the tempo was too slow or too fast
        :param duration_errors: dict
        :return: dict
        """
        measures = {}
        for time in duration_errors:
            current_measure = 0
            for key, measure in self.measures.items():
                current_measure = key
                if measure['msecond_start'] > time:
                    break
            error_message = 'Wrong tempo'
            measures[current_measure] = error_message
        return measures

    def map_note_errors_to_measures(self, errors: dict) -> list:
        """
        Maps notes errors to the object that contains measure and note id
        that contain error
        :param errors: dict
        :return: dict
        """
        notes_to_update = []
        for note in self.notes:
            for err_second, err_notes in sorted(errors.items()):
                if not (note.starts <= err_second <= note.ends):
                    continue
                # if note['type'] == 'pause':
                #     notes_to_update.append(note['id'])
                #     continue
                for err_note in err_notes:
                    if (note.has_in_pitches(err_note)):
                        notes_to_update.append(note.id)
        return notes_to_update

    def save_file(self, output_path: str):
        """
        Saves output mxl file
        :param output_path:
        """
        self.stream.write('mxl', fp=output_path)

    def convert_to_midi(self, filepath: str, bpm: int) -> str:
        """
        Converts input file to midi using music21 library
        :param bpm: int
        :return:
        """
        self.stream = parse(filepath)
        self._reveal_notes(self.stream, bpm)

        midi_file = os.path.splitext(filepath)[0] + ".mid"
        self.stream.write('midi', fp=midi_file)

        return midi_file

    def convert_to_wav(self, midi_file_path: str) -> str:
        """
        Uses fluidsynth to output the midi file into an wav file
        :param midi_file: str
        :param output_file: str
        :param fs: int
        """

        temp_wav = os.path.splitext(midi_file_path)[0] + ".wav"
        sound_front2_path = os.path.join(os.path.dirname(
            pretty_midi.__file__),
            pretty_midi.DEFAULT_SF2)

        with open(os.devnull, 'w') as devnull:
            fs = FluidSynth(sound_font=sound_front2_path,
                        sample_rate=self.AUDIO_FS)
            fs.midi_to_audio(midi_file=midi_file_path, audio_file=temp_wav)

        return temp_wav

    def apply_errors(self, note_errors, duration_errors):
        note_errors = self.map_note_errors_to_measures(note_errors)
        duration_errors = self.map_duration_errors_to_measures(duration_errors)
        self.inject_errors(note_errors, duration_errors)
        head, tail = os.path.split(self.filepath)
        filename = head + "/generated_" + tail
        self.save_file(filename)

        return filename
