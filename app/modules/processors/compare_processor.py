import librosa
import librosa.core
import numpy
from fastdtw import fastdtw
from scipy.spatial.distance import cosine


class CompareProcessor(object):
    AUDIO_FS = 22050
    MIDI_HOP = 512
    NOTE_START = 20
    N_NOTES = 48
    ERROR_THRESHOLD = 0.6
    FILTER_THRESHOLD = 0.6

    def _extract_features(self,
                          audio_file_path: str,
                          duration: float = 20.0) -> tuple:
        """

        :param audio_file_path:
        :param duration:
        :return:
        """
        signal, sample_rate = librosa.load(audio_file_path,
                                           duration=duration)
        chroma_features = librosa.feature.chroma_cqt(
            signal,
            sr=sample_rate,
            hop_length=self.MIDI_HOP)

        chroma_features = self._filter_features(chroma_features)
        # we need only transposed features
        return signal, sample_rate, chroma_features.T

    def _filter_features(self, features):
        """

        :param features:
        :return:
        """
        features[features < 0.6 * features.max()] = 0
        return features

    def _extract_alignment_distance(self,
                                    features_aT: numpy.ndarray,
                                    features_bT: numpy.ndarray):
        """

        :param features_aT: numpy.ndarray (chroma or cens features)
        :param features_bT: numpy.ndarray (chroma or cens features)
        :return: tuple
        """
        return fastdtw(x=features_aT, y=features_bT,
                       dist=cosine)

    def __compare_features(self,
                           features_aT: numpy.ndarray,
                           features_bT: numpy.ndarray,
                           path) -> numpy.ndarray:
        """

        :param features_aT: numpy.ndarray (chroma or cens features)
        :param features_bT: numpy.ndarray (chroma or cens features)
        :param path: list[tuple]
        :return:
        """
        compared_notes = []

        for item in path:
            note_distance = features_aT[item[0]] - features_bT[item[1]]
            note_distance = map(
                lambda x: x if abs(x) >= self.ERROR_THRESHOLD else 0.0,
                note_distance)
            compared_notes.append(list(note_distance))

        return numpy.array(numpy.transpose(compared_notes))

    def _get_notes_errors(self,
                          features_aT: numpy.ndarray,
                          features_bT: numpy.ndarray,
                          path,
                          sampling_rate):
        """

        :param features_aT:
        :param features_bT:
        :param path:
        :param sampling_rate:
        :return:
        """

        compared_notes = self.__compare_features(features_aT, features_bT,
                                                 path)

        del features_aT
        del features_bT

        pathIndex = 0

        duration_errors = []
        while pathIndex < len(path):
            dist = path[pathIndex][0] - path[pathIndex][1]
            if abs(dist) > path[pathIndex][0] / 2:
                duration_errors.append(pathIndex)
            pathIndex += 30

        note_errors = {}

        cols = compared_notes.shape[0]
        rows = compared_notes.shape[1]

        notes = []
        time_list = []
        for x in range(0, rows):
            notes.append(librosa.core.midi_to_note(x).replace('-', ''))

        for y in range(0, rows):
            time_list.append(
                1000 * librosa.frames_to_time(y, sr=sampling_rate,
                                              hop_length=self.MIDI_HOP))

        for x in range(0, cols - 10):
            for y in range(10, rows - 10):
                if compared_notes[x, y] == 0:
                    continue
                note_errors.setdefault(time_list[y], []).append(notes[x])

        duration_errors = map(lambda x: time_list[x], duration_errors)
        return note_errors, duration_errors

    def compare(self,
                audio_file_path: str,
                midi_file_path: str,
                duration=20.0):

        signal_midi, sample_rate_midi, features_midi = self._extract_features(
            midi_file_path, duration)
        signal_wav, sample_rate_wav, features_wav = self._extract_features(
            audio_file_path, duration)

        # transposing_features
        features_midi_t = features_midi.T
        features_wav_t = features_wav.T

        distance, path = self._extract_alignment_distance(features_midi_t,
                                                          features_wav_t)
        note_errs, duration_err = self._get_notes_errors(
            features_midi_t,
            features_wav_t,
            path,
            sample_rate_midi)

        return note_errs, duration_err
