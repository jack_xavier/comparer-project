# from nameko.rpc import rpc


class AudioProcessor(object):
    name = "audio_processor"

    # TODO : Log DB normalisation
    def normalise(self):
        """
        Normalisation
        """
        pass

    # TODO : Remove metronome
    def remove_metronome(self):
        pass

    # TODO: Preprocess Audio
    def process(self, filepath: str) -> str:
        return filepath