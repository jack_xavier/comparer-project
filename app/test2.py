import librosa
import librosa.core
import librosa.display
import numpy
from fastdtw import fastdtw
from music21 import tempo
from music21.converter import parse
from music21.repeat import ExpanderException
from music21.stream import Measure
from music21.stream import Stream
from music21.stream import repeat
from scipy.spatial.distance import cosine
from music21.note import Note, Rest
from music21.chord import Chord

AUDIO_FS = 22050
AUDIO_HOP = 1024
MIDI_FS = 11025
MIDI_HOP = 512
NOTE_START = 20
N_NOTES = 80


class Tact(object):
    def __init__(self,
                 id: int = 0,
                 starts: float = .0,
                 duration: float = .0,
                 duration_in_ms=.0,
                 ends: float = .0,
                 staff: int = 0):
        self.id = id
        self.starts = starts
        self.duration = duration
        self.duration_in_ms = duration_in_ms
        self.ends = ends
        self.staff = staff


class NoteElement(object):
    def __init__(self,
                 type: str = 'pause',
                 id: int = 0,
                 starts: float = .0,
                 ends: float = .0,
                 measure: int = 0):
        self.type = type
        self.id = id
        self.starts = starts
        self.ends = ends
        self.measure = measure
        self.pitches = ''

    @property
    def pitches(self) -> str:
        return self._pitches

    @pitches.setter
    def pitches(self, pitches_string: str):
        self._pitches = pitches_string.replace('-', '')

    def has_in_pitches(self, note: str):
        return note in self._pitches


class MusicInformationCollection(object):
    def __init__(self, stream: Stream, measure_from,measure_to ,bpm=120):
        self.stream = stream
        self.measures = []
        self.notes = []
        self.bpm = bpm
        self.metronomeMark = tempo.MetronomeMark(number=self.bpm,
                                                 referent='half')

    def process_notes(self):
        notes = []
        measures = []
        for staff in self.stream:
            if not staff.isStream:
                continue
            staff = self.expand_stream(staff)
            measures, notes = self.notes_info(staff,measures,notes)
        self.notes = sorted(notes)
        self.measures = sorted(measures)

        return  self.measures, self.notes

    def expand_stream(self, stream: Stream):
        try:
            expander = repeat.Expander(stream)
            song = expander.process()
        except ExpanderException as ex:
            song = stream
        return song

    def notes_info(self, staff: Stream, measures, notes):
        measures_timing = filter(lambda x: x['element'].isStream,
                                 staff.secondsMap)
        for measure in measures_timing:
            measure_object = measure['element']
            self.measures.append(Tact(
                id=measure_object.id,
                starts=measure['offsetSeconds'],
                ends=measure['endTimeSeconds'],
                staff=staff.id
            ))
            notes_timing = filter(
                lambda x: isinstance(x['element'], Chord) or isinstance(
                    x['element'], Rest) or isinstance(x['element'], Note),
                staff.secondsMap)
            for _note in notes_timing:
                note_object = _note['element']
                note = NoteElement(
                    starts=_note['offsetSeconds'],
                    ends=_note['endTimeSeconds'],
                    id=note_object.id,
                    measure=measure_object.id)

                if note_object.isNote:
                    note.type = 'note'
                    note.pitches = str(note_object.pitch)

                if note_object.isChord:
                    note.type = 'chord'
                    note.pitches = ' '.join(
                        [str(x.pitch) for x in note_object._notes])

                notes.append(note)
        return measures, notes

    def extract_measures(self, stream: Stream):
        for measure in stream:
            if not measure.isStream:
                continue
            measure.insert(0,
                           tempo.MetronomeMark(self.bpm, referent='half'))
            if not self.measures.get(measure.id):
                self.measures[measure.id] = measure.seconds
                self.measures[measure.id]['elements'] = measure.secondsMap
            else:
                self.measures[measure.id]['elements'] += \
                    measure.secondsMap[
                        'elements']


def compare_lengths(midi_file, audio_file):
    x1, sr1 = librosa.load(midi_file, duration=20.0)
    x2, sr2 = librosa.load(audio_file, duration=20.0)

    C1_cens = librosa.feature.chroma_cqt(
        x1, sr=sr1, hop_length=MIDI_HOP)
    C2_cens = librosa.feature.chroma_cqt(
        x2, sr=sr2, hop_length=MIDI_HOP)

    distance, path = fastdtw(x=C1_cens.T, y=C2_cens.T, dist=cosine)

    C1_cens[C1_cens < 0.6 * C1_cens.max()] = 0
    C2_cens[C2_cens < 0.6 * C2_cens.max()] = 0

    C1_chroma = C1_cens.T
    C2_chroma = C2_cens.T

    compared_notes = []

    last_item = None
    for item in path:
        note_distance = C1_chroma[item[0]] - C2_chroma[item[1]]
        note_distance = map(lambda x: x if abs(x) >= 0.5 else 0.0,
                            note_distance)
        compared_notes.append(list(note_distance))
        last_item = item[0]

    seconds = 1000 * 20.0 / last_item

    compared_notes = numpy.array(numpy.transpose(compared_notes))

    errors = {}

    cols = compared_notes.shape[0]
    rows = compared_notes.shape[1]

    # del C1_cens
    # del C2_cens
    # del C1_chroma
    # del C2_chroma

    notes = [librosa.core.midi_to_note(x, octave=False).replace('-', '') for
             x
             in
             range(0, cols)]

    time_list0 = []
    for y in range(10, rows):
        time_list0.append(
            1000 * librosa.frames_to_time(path[y][0], hop_length=MIDI_HOP))

    time_list = []
    for y in range(10, rows):
        time_list.append(path[y][0] * seconds)

    for x in range(0, cols):
        for y in range(0, rows - 10):
            if compared_notes[x, y] == 0:
                continue
            errors.setdefault(time_list[y], []).append(notes[x])

    return errors


def reveal_notes(stream, bpm):
    measures = {}
    notes = []
    for element in stream:
        if element.isStream:
            notes, measures = process_stream(element, bpm, notes, measures)
    return notes, measures


def process_stream(sream, bpm, notes, measures):
    try:
        expander = repeat.Expander(sream)
        song = expander.process()

    except ExpanderException as ex:
        song = sream

    timing = song.secondsMap

    total_duration = 0.0
    milliseconds_per_beat = 60000.0 / float(bpm)

    for measureStream in song:
        if not measureStream.isStream:
            continue

        for element in measureStream:
            if isinstance(element, tempo.MetronomeMark):
                milliseconds_per_beat = 60000.0 / float(
                    element.getQuarterBPM())

        if measureStream.number not in measures:
            measure = {
                'msecond_start': total_duration * milliseconds_per_beat,
                'duration': 0.0,
                'duration_in_ms': 0.0,
                'elements': [],
                'number': measureStream.number
            }
        else:
            measure = measures[measureStream.number]

        starts = measure['msecond_start']
        for a in measureStream.recurse().notesAndRests:
            length = float(
                a.duration.quarterLength) if a.duration.quarterLength else 0.0
            measure['duration'] += length
            ends = length * milliseconds_per_beat

            element = NoteElement(
                starts=starts,
                ends=starts + ends,
                id=a.id,
                measure=measureStream.number)

            if a.isNote:
                element.type = 'note'
                element.pitches = str(a.pitch)

            if a.isChord:
                element.type = 'chord'
                element.pitches = ' '.join(
                    [str(x.pitch) for x in a._notes])

            measure['elements'].append(element)
            notes.append(element)
            starts += ends * milliseconds_per_beat

        total_duration += measure['duration']
        measure['duration_in_ms'] = measure[
                                        'duration'] * milliseconds_per_beat
        measures[measureStream.number] = measure
    return notes, measures


def add_errors_to_file(stream, notes_issues):
    # for part
    for part in stream:
        if not part.isStream:
            continue
        for measure in part:
            if not isinstance(measure, Measure):
                continue

            # if measure.number in tempo_issues.keys():
            #     measure.removeByClass(TextExpression)
            #     measure.append(TextExpression(tempo_issues[measure.number]))
            for a in measure.elements:
                if a.id in notes_issues:
                    a.addLyric('er')

    return stream


def map_duration_erros_to_measures(notesInformation, duration_errors):
    measures = {}
    for time, error in duration_errors.items():
        current_measure = 0
        for key, measure in notesInformation.items():
            current_measure = key
            if measure['msecond_start'] > time:
                break
        error_message = 'Too slow' if error < 1 else 'Too fast'
        measures[current_measure] = error_message
    return measures


def map_note_erros_to_measures(notesInformation: dict, errors: dict):
    measures = {}
    for key, measure in notesInformation.items():
        measure_start = measure['msecond_start']
        elements = measure['elements']
        sorted(elements, key=lambda i: i['starts'])

    for key, measure in notesInformation.items():
        measure_start = measure['msecond_start']
        measure_end = measure_start + measure['duration_in_ms']
        notes = []
        for error_key, error in errors.items():
            if measure_start <= error_key <= measure_end:
                note_time_offset = error_key - measure_start
                for note in measure['elements']:
                    if note['starts'] > note_time_offset:
                        continue
                    if (note['type'] == 'note' or note['type'] == 'chord') \
                            and error[0] in note['pitches']:
                        notes.append(note['id'])
            if notes:
                measures[key] = notes
    return measures


# durationPath = {
#     345000.487: 0.5,
#     1678445.00: 1.3
# }

# durationPath = {}
#
# for key, measure in notesInformation.items():
#     if not (bool(random.getrandbits(1))):
#         continue
#     time = measure['msecond_start'] + (random.random() * 10000)
#     speed = random.uniform(0.3, 1.8)
#     durationPath[time] = speed
#
# errorsPath = []
#
# for key, measure in notesInformation.items():
#     if not (bool(random.getrandbits(1))):
#         continue
#
#     for i in range(1, 5):
#         time = random.uniform(measure['msecond_start'],
#                               measure['msecond_start'] + 333333.33)
#         errorsPath.append(time)
#
# tempo_issues = map_duration_erros_to_measures(notesInformation, durationPath)
# errors_issues = map_note_erros_to_measures(notesInformation, errorsPath)
# streamPiano = add_errors_to_file(streamPiano, tempo_issues, errors_issues)
# streamPiano.write('mxl',
#                   fp='/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven_generated.mxl')

# midiFile, midi_info = transformMxl(
#     input_path='/Users/evgeniipetrov/personal/comparator/instance/uploads/Rachmaninoff.mxl',
#     output_path='/Users/evgeniipetrov/personal/comparator/instance/uploads/Rachmaninoff.mid',
#     bpm=72
# )

# midi_feat = transformMidi(
#     midi_file='/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven.mid',
#     output_file='/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven_mxml.wav',
#     duration=10.0)

# audio_feat = transformAudio(
#     audio_path='/Users/evgeniipetrov/personal/comparator/instance/uploads/rachmaninov.wav',
#     duration=10.0
# )
#

streamPiano = parse(
    '/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven.mxl')

# collection = MusicInformationCollection(streamPiano).process_notes()
notes_info, measures_info = reveal_notes(streamPiano, 120)

notes_info = sorted(notes_info, key=lambda i: i.starts)
# secondsMap = streamPiano.secondsMap
#
# errors = compare_lengths(
#     audio_file='/Users/evgeniipetrov/personal/comparator/instance/uploads/output.wav',
#     midi_file='/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven_mxml.wav')

# current_note_iteration = 0
# sorted_errors = sorted(errors.items())
# notes_to_update = []
# for note in notes_info:
#     for err_second, err_notes in sorted_errors:
#         if not (note.starts <= err_second <= note.ends):
#             continue
#         # if note['type'] == 'pause':
#         #     notes_to_update.append(note['id'])
#         #     continue
#         for err_note in err_notes:
#             if (note.has_in_pitches(err_note)):
#                 notes_to_update.append(note.id)
#
# notes_to_update = list(numpy.unique(notes_to_update))
# streamPiano = add_errors_to_file(streamPiano, notes_to_update)
# streamPiano.write('mxl',
#                   fp='/Users/evgeniipetrov/personal/comparator/instance/uploads/Beethoven_generated.mxl')
