from flask import Flask
from flasgger import Swagger
from app.config import Config

app = Flask(__name__)
swagger = Swagger(app)

app.config.from_object(Config)

from app import routes, errors
from app.modules.compare.controllers import comparer

app.register_blueprint(comparer)
