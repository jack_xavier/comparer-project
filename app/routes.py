from flask import make_response, jsonify

from app import app


@app.route('/')
@app.route('/index')
@app.route('/health')
def index_action():
    return make_response(jsonify({
        'status': 'connection_ok',
    }))
