import logging

from app import app

app.run(debug=True)
app.logger.setLevel(logging.INFO)

