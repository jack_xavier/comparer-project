.DEFAULT_GOAL := help
.PHONY: help init update docs up down restart logs test-up test-down test test-run test-clean watch cs fix-perms

help: ## Show this help (default)
	@echo "Usage: make [command] [args=\"\"] \n"
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-15s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init: ## Initialize whole local development environment
	cp -n .env.dist .env
	$(MAKE) up
	.docker/update.sh

update: SHELL := /bin/bash
update: ## Updates project after changes
	docker-compose build app;
	@if [ -z "$(FE)" ]; then \
		echo -e "\033[1;31mUse 'FE=true make update' to build frontend as well\033[0m"; \
	else \
		rm -rf public/static/*; \
		.docker/build-npm.sh; \
	fi
	$(MAKE) up
	.docker/update.sh
	@echo -e "\033[1;31mDon't forget to sync .env file!\033[0m"

up: ## Start docker environment
	docker-compose up -d

down: ## Shutdown docker environment
	docker-compose down --remove-orphans --volumes

restart: down up ## Stop and start docker environment
